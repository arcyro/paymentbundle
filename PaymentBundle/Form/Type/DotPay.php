<?php

namespace Arcyro\PaymentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DotPay extends AbstractType {

    private $container;
    private $locale;

    public function __construct($container) {
        $this->container = $container;
        $this->locale = $this->container->get('request')->getLocale();
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $dotpay = $this->container->getParameter('dotpay');
        $session = $this->container->get('session')->get('pay');
        $preUrl = 'http://' . $this->container->get('request')->server->get("HTTP_HOST");
        $locale = $this->locale;
        $currency = function () use ($locale) {
            $currencies = array(
                'en_US' => "USD",
                'en_GB' => "GBP",
                'pl' => 'PLN'
            );

            if (in_array($locale, array_keys($currencies))) {
                return $currencies[$locale];
            } else {
                return 'USD';
            }
        };

        $builder
                ->setAction($this->container->get('router')->generate('arcyro_payment_homepage'))
                ->setMethod('POST')
                ->add('id', 'hidden', array('data' => $this->container->get('kernel')->getEnvironment() != 'prod' ? $dotpay['id'] : $dotpay['id']))
                ->add('amount', 'hidden')
                ->add('currency', 'hidden', array('data' => $currency()))
                ->add('email', 'hidden', array('data' => $this->container->get('security.context')->getToken()->getUser()->getEmail()))
                ->add('subscription', 'hidden', array('data' => $session['interval'], 'attr' => array('class' => 'subscription-field', 'required' => false)))
                ->add('advertSubscription', 'hidden', array('attr' => array('class' => 'advert-subscription-field', 'required' => false)))
                ->add('userid', 'hidden', array('data' => $this->container->get('security.context')->getToken()->getUser()->getId()))
                ->add('description', 'hidden', array('attr' => array('class' => 'dot-pay-description')))
                ->add('lang', 'hidden', array('data' => $this->container->get('request')->getLocale()))
                ->add('control', 'hidden')
                ->add('street', 'hidden')
                ->add('ch_lock', 'hidden', array('data' => 1))
                ->add('channel', 'hidden', array('attr' => array('class' => 'dotpayChannel')))
                ->add('street_n1', 'hidden')
                ->add('street_n2', 'hidden')
                ->add('phone', 'hidden')
                ->add('postcode', 'hidden')
                ->add('URL', 'hidden')
                ->add('URLC', 'hidden', array('data' => $preUrl . $this->container->get('router')->generate('arcyro_payment_result')))
                ->add('type', 'hidden', array('data' => 3))
                ->add('buttontext', 'hidden', array('data' => $dotpay['btn']))
                ->add('firstname', 'hidden', array('data' => $this->container->get('security.context')->getToken()->getUser()->getFirstname()))
                ->add('lastname', 'hidden', array('data' => $this->container->get('security.context')->getToken()->getUser()->getLastname()));
    }

    public function getName() {
        return 'arcyro_paymentbundle_dotpay';
    }

}
