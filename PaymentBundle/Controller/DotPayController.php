<?php

namespace Arcyro\PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Arcyro\BaseBundle\Interfaces\InitControllerInterface;
use Arcyro\PaymentBundle\Form\Type\DotPay;
use Arcyro\UserBundle\Entity\UserSubscription;
use Arcyro\PaymentBundle\Entity\Transaction;
use Arcyro\UserBundle\Doctrine\UserManager;

class DotPayController extends Controller {

    private $return = array();

    /**
     * @var Request
     */
    public $request;

    /**
     * @var Array
     */
    private $dotpay;

    /**
     * @var ContainerAware
     */
    protected $container;

    public function __construct($container = null) {
        $this->container = $container;
        if ($this->container) {
            $this->dotpay = $this->container->getParameter('dotpay');
            $this->request = $this->getRequest();
        }
    }

    public function init() {
        $this->request = $this->container->get('request');
        $this->dotpay = $this->container->getParameter('dotpay');
    }

    /**
     * Akcja płatnośći za abonament sprzedawcy w serwisie
     *  @Template
     */
    public function indexAction() {
        $form = $this->createForm(new DotPay($this->container));
        $form->setData($this->request->request->all());
        $this->return['form'] = $form->createView();
        if ($this->request->isMethod('POST') && !$this->request->get('interval', false)) {
            $dotpay = $this->container->getParameter('dotpay');
            $data = $this->request->request->all();
            $data = $data[$form->getName()];

            $em = $this->getDoctrine()->getEntityManager();
            $now = new \DateTime('now');
            $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

            $activeSubscriptions = $this->getDoctrine()->getManager()
                    ->getRepository('ArcyroUserBundle:UserSubscription')
                    ->createQueryBuilder('s')
                    ->where($qb->expr()->eq('s.user', $this->getUser()->getId()))
                    ->andWhere($qb->expr()->eq('s.active', true))
                    ->andWhere($qb->expr()->gt('s.subscriptionTo', $now->format('Y-m-d')))
                    ->orderBy('s.subscriptionTo', 'DESC')
                    ->getQuery()
                    ->execute();
            $add = 0;

            foreach ($activeSubscriptions as $s) {
                $interval = $now->diff($s->getSubscriptionTo());
                $add += $interval->format('%h');
                $s->setActive(false);
                $this->getDoctrine()->getManager()->flush();
            }


            $subscription = $this->getDoctrine()->getManager()
                    ->getRepository('ArcyroBaseBundle:SubscriptionInterval')
                    ->find($data['subscription']);
            $user = $em->getRepository('ArcyroUserBundle:User')->find($this->getUser()->getId());
            if (count($activeSubscriptions) > 0) {
                $toDate = $activeSubscriptions[0]->getSubscriptionTo();
            } else {
                $toDate = new \DateTime();
            }
            $interval = new \DateInterval('PT' . ($subscription->getDays() * 24) . 'H');
            $toDate->add($interval);
            $newDate = clone $toDate;
            $transaction = new Transaction();
            $transaction->setType('dotpay');
            $transaction->setPrice($subscription->getPrice());
            $userSubscription = new UserSubscription();
            $userSubscription->setActive(false);
            $userSubscription->setUser($user);
            $userSubscription->setTransaction($transaction);
            //$userSubscription->setSubscriptionTo($toDate);
            $userSubscription->setSubscription($subscription);
            $em->persist($transaction);
            $em->persist($userSubscription);
            $em->flush();

            $tmp = array(
                'id' => $data['id'],
                'amount' => $subscription->getPrice() / 100,
                'currency' => $data['currency'],
                'lang' => $data['lang'],
                'URL' => 'http://' . $this->getRequest()->server->get('HTTP_HOST') . $this->generateUrl('arcyro_payment_end', array('userSubscriptionId' => $userSubscription->getId(), 'type' => 'subscription')),
                'URLC' => $data['URLC'],
                'type' => $data['type'],
                'buttontext' => $data['buttontext'],
                'firstname' => $data['firstname'],
                'kanal' => $data['channel'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'description' => $this->container->get('translator')->trans('h1.pay.for.subscription'),
                'control' => $userSubscription->getId() . '-us-time-' . $newDate->getTimestamp(),
                'country' => 'POL',
                'blokuj' => 1,
                'ch_lock' => 1,
                'type' => 4, //ten parametr od razu przekierowuje do banku
                'city' => method_exists($user, 'getCity') ? $user->getCity() : '',
                'phone' => method_exists($user, 'getPhone') ? $user->getPhone() : '',
                'street' => method_exists($user, 'getStreet') ? $user->getStreet() : '',
                'street_n1' => method_exists($user, 'getHomeNumber') ? $user->getHomeNumber() : '',
                'street_n2' => method_exists($user, 'getFlatNumber') ? $user->getFlatNumber() : '',
                'postcode' => method_exists($user, 'getPostcode') ? $user->getPostcode() : '',
            );
            if (in_array($this->container->get('kernel')->getEnvironment(), array('test', 'dev'))) {
                //unset($tmp['kanal']);
                //unset($tmp['blokuj']);
                // $tmp['id'] = $dotpay['test_id'];
            }
            $query = http_build_query($tmp);
            $this->get('dotpay.logger')->alert($query);
            return $this->redirect($dotpay['uri'] . '?' . $query);
        }
        return $this->renderView('ArcyroPaymentBundle:DotPay:index.html.twig', $this->return);
    }

    /**
     *  @Template
     */
    public function endAction() {
        $this->container->get('session')->remove('access_denied');
        return $this->return;
    }

    public function resultAction() {
        if ($this->request->isMethod('POST')) {
            $this->get('dotpay.logger')->alert(serialize($this->request->request->all()));
            $control = $this->request->get('control');
            $isMatch = preg_match('/^[0-9]+\-us\-time\-[0-9]+$/', $control);
            preg_match('/^[0-9]+/', $control, $matchId);
            preg_match('/[0-9]+$/', $control, $matchTime);
            if ($isMatch && isset($matchId[0]) && is_numeric($matchId[0])) {
                $userSubscription = $this->getDoctrine()->getManager()
                        ->getRepository('ArcyroUserBundle:UserSubscription')
                        ->find($matchId[0]);
            }
            if ($userSubscription) {
                if ($this->request->get('status') == 'OK') {
                    $userSubscription->setActive(true);
                    $userSubscription->getTransaction()->setStatus(2);
                    $userSubscription->getTransaction()->setNumberInYear($this->getDoctrine()->getManager()
                                    ->getRepository('ArcyroPaymentBundle:Transaction')
                                    ->getNextInvoiceNumberInYear());
                } else {
                    $userSubscription->getTransaction()->setStatus(1);
                }
                $subscriptionTo = new \DateTime();
                $subscriptionTo->setTimestamp($matchTime[0]);
                $userSubscription->setSubscriptionTo($subscriptionTo);
                $userSubscription->getUser()->setValidTo($subscriptionTo);

                $data = $this->request->request->all();
                unset($data['description']);
                $userSubscription->getTransaction()->setParams($data);
                if ($userSubscription->getUser()->hasRole("ROLE_HOTEL")) {
                    if (!$userSubscription->getUser()->hasRole("ROLE_HOTEL_PAID")) {
                        $userSubscription->getUser()->addRole("ROLE_HOTEL_PAID");
                    }
                }
                $this->getDoctrine()->getManager()->flush();
                $this->container->get('security.context')->getToken()->setAuthenticated(false);
            }
            exit('OK');
        } else {
            exit('ERROR');
        }
    }

    public function checkPayAction() {
        if ($this->getRequest()->isMethod('POST')) {
            if ($this->getRequest()->get('type') == 'subscription') {
                $userSubscription = $this->getDoctrine()->getManager()
                        ->getRepository('ArcyroUserBundle:UserSubscription')
                        ->find($this->request->get('id'));
            }
            if ($userSubscription->getActive()) {
                $this->container->get('security.context')->getToken()->setAuthenticated(false);
                return new Response(json_encode(array('result' => true)), 200, array('Content-Type' => 'application/json'));
            } else {
                return new Response(json_encode(array('result' => false)), 200, array('Content-Type' => 'application/json'));
            }
        }
    }

    public function checkCode($check, $dotpayId, $checkType, $disposableCode, $intervalType) {
        try {
            $array = array();
            $array['check'] = $check;
            $array['code'] = str_replace('AP.', '', $intervalType->getSmsBody());
            $array['id'] = $dotpayId;
            $array['type'] = $checkType;
            $array['del'] = 0; //$disposableCode;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://ssl.dotpay.pl/check_code.php");
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 100);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array);
            $recv = curl_exec($ch);
            $error = curl_error($ch);
            curl_close($ch);
            if ($error) {
                $this->get('dotpay.logger')->error($error . ' | params: ' . serialize($array));
                return false;
            }
            $dane = explode("\n", $recv);

//            if($_SERVER['REMOTE_ADDR'] == '83.24.236.156'){
//                echo '<pre>'; print_r($array); echo '<br/>';
//                echo '<pre>'; print_r($dane); die;
//            }

            $status = $dane[0];
            if ($status == 0) {
                $this->get('dotpay.logger')->error('Code status is not valid for data: ' . serialize($array));
                return false; //error - not valid code
            } else {  //valid code
                $em = $this->getDoctrine()->getEntityManager();
                $now = new \DateTime('now');
                $qb = $this->getDoctrine()->getManager()->createQueryBuilder();

                $activeSubscriptions = $this->getDoctrine()->getManager()
                        ->getRepository('ArcyroUserBundle:UserSubscription')
                        ->createQueryBuilder('s')
                        ->where($qb->expr()->eq('s.user', $this->getUser()->getId()))
                        ->andWhere($qb->expr()->eq('s.active', true))
                        ->andWhere($qb->expr()->gt('s.subscriptionTo', $now->format('Y-m-d')))
                        ->orderBy('s.subscriptionTo', 'DESC')
                        ->getQuery()
                        ->execute();
                $add = 0;

                foreach ($activeSubscriptions as $s) {
                    $interval = $now->diff($s->getSubscriptionTo());
                    $add += $interval->format('%h');
                    $s->setActive(false);
                    $this->getDoctrine()->getManager()->flush();
                }

                if (count($activeSubscriptions) > 0) {
                    $toDate = $activeSubscriptions[0]->getSubscriptionTo();
                } else {
                    $toDate = new \DateTime();
                }
                $interval = new \DateInterval('PT' . ($intervalType->getDays() * 24) . 'H');
                $toDate->add($interval);
                $newDate = clone $toDate;
                $transaction = new Transaction();
                $transaction->setType('dotpay');
                $transaction->setPrice($intervalType->getPrice());
                $userSubscription = new UserSubscription();
                $userSubscription->setActive(true);
                $userSubscription->setUser($this->getUser());
                $userSubscription->setTransaction($transaction);
                $userSubscription->setSubscriptionTo($newDate);
                $userSubscription->setSubscription($intervalType);
                $em->persist($transaction);
                $em->persist($userSubscription);
                $userSubscription->getUser()->setValidTo($toDate);
                if ($userSubscription->getUser()->hasRole("ROLE_HOTEL")) {
                    if (!$userSubscription->getUser()->hasRole("ROLE_HOTEL_PAID")) {
                        $userSubscription->getUser()->addRole("ROLE_HOTEL_PAID");
                    }
                }
                $em->flush();

                return true;
            }
        } catch (\Exception $e) {
            $this->get('dotpay.logger')->error($e->getMessage() . ' | params: ' . serialize($array));
            return false;
        }
    }

    public function getDotpayChannels() {
        $dotpay = $this->container->getParameter('dotpay');
        $hash = md5($dotpay['pin'] . $dotpay['id'] . ($this->request->getLocale() == 'pl' ? 'pl' : 'en'));
        $obj = $this;
        $getChannels = function() use($dotpay, $hash, $obj) {
            $content = file_get_contents('https://ssl.dotpay.pl/api/availableChannels/?id=' . $dotpay['id'] . '&language=' . ($obj->request->getLocale() == 'pl' ? 'pl' : 'en') . '&hash=' . $hash);
            $content = json_decode($content, true);
            return $content;
        };

        return $getChannels();
    }

}
