<?php

namespace Arcyro\PaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Transaction
 *
 * @ORM\Table(name="transaction")
 * @ORM\Entity(repositoryClass="Arcyro\PaymentBundle\Entity\Repository\TransactionRepository")
 */
class Transaction {

    /**
     * @var integer

     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint", options={"default" = 0})
     */
    protected $status = 0;

    /**
     * @var array
     *
     * @ORM\Column(name="params", type="array")
     */
    protected $params;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="type_transaction", type="string", length=50)
     */
    protected $typeSubscription = 'subscription';

    /**
     * @ORM\OneToOne(targetEntity="Arcyro\UserBundle\Entity\UserSubscription", mappedBy="transaction")
     */
    private $userSubscription;

    /**
     * @var string
     * @ORM\Column(name="price", type="decimal", length=10)
     * @Assert\NotBlank()
     */
    protected $price;

    /**
     * @var string
     *
     * @ORM\Column(name="numberInYear", type="string", length=50)
     */
    protected $numberInYear;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Transaction
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Transaction
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Set userSubscription
     *
     * @param \Arcyro\UserBundle\Entity\UserSubscription $userSubscription
     * @return Transaction
     */
    public function setUserSubscription(\Arcyro\UserBundle\Entity\UserSubscription $userSubscription = null) {
        $this->userSubscription = $userSubscription;

        return $this;
    }

    /**
     * Get userSubscription
     *
     * @return \Arcyro\UserBundle\Entity\UserSubscription 
     */
    public function getUserSubscription() {
        return $this->userSubscription;
    }

    /**
     * Set params
     *
     * @param string $params
     * @return Transaction
     */
    public function setParams($params) {
        $this->params = $params;

        return $this;
    }

    /**
     * Get params
     *
     * @return string 
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Transaction
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Transaction
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set typeSubscription
     *
     * @param string $typeSubscription
     * @return Transaction
     */
    public function setTypeSubscription($typeSubscription) {
        $this->typeSubscription = $typeSubscription;

        return $this;
    }

    /**
     * Get typeSubscription
     *
     * @return string 
     */
    public function getTypeSubscription() {
        return $this->typeSubscription;
    }


    /**
     * Set numberInYear
     *
     * @param string $numberInYear
     * @return Transaction
     */
    public function setNumberInYear($numberInYear)
    {
        $this->numberInYear = $numberInYear;
    
        return $this;
    }

    /**
     * Get numberInYear
     *
     * @return string 
     */
    public function getNumberInYear()
    {
        return $this->numberInYear;
    }
}